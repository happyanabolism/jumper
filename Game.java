//
//
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

// The main class
public class Game extends Application {
	
	// Panels
	static Pane appRoot;
	static Pane gameRoot;
	static ScorePanel scorePanel;
	
	// Player and platforms
	Jumper jumper;
	public static ArrayList<Platform> platforms = new ArrayList<Platform>();
	public static ArrayList<Bonus> bonuses = new ArrayList<Bonus>();
	
	// Pressed keys
	public static HashMap <KeyCode, Boolean> keys = new HashMap<>();
	
	// Window's sizes and background
	public static final int windowWidth = 600;
	public static final int windowHeight = 600;
	private final Image imgBackground = new Image(getClass().getResource("background.png").toString(),
												  windowWidth, windowHeight, false, false);
	private ImageView background;
	AnimationTimer timer;
	
	private void generateBonus(Platform platform) {
		boolean yesOrNot = new Random().nextBoolean();
		if (!yesOrNot) { return; }
		
		Bonus bonus;
		double probability = new Random().nextDouble();
		

		if (probability < Jetpack.probability) {
			bonus = new Jetpack(platform);
		}
		else if (probability < GoldCoin.probability) {
			bonus = new GoldCoin(platform);
		}
		else if (probability < SilverCoin.probability) {
			bonus = new SilverCoin(platform);
		}
		else {
			bonus = new BronzeCoin(platform);
		}
		
		Game.bonuses.add(bonus);
		Game.gameRoot.getChildren().add(bonus);
	}
	
	private void generatePlatforms(int count) {
		boolean isSmall;
		
		for (int i = 0; i < count; i++) {
			isSmall = new Random().nextBoolean();
			
			Platform platform;
			if (isSmall) { platform = new SmallPlatform(); }
			else { platform = new BigPlatform(); }
			
			int x = new Random().nextInt(windowWidth - platform.getPlatformWidth());
			platform.setTranslateX(x);
			platform.setTranslateY(600 - i * 110 - 120);
			platforms.add(platform);
			
			generateBonus(platform);
		}
		
		gameRoot.getChildren().addAll(platforms);
	}

	public void moveGameRoot() {
		gameRoot.setLayoutY(gameRoot.getLayoutY() + jumper.getScore() / 10);
	}
	
	public static boolean isPressed(KeyCode key) {
		return keys.getOrDefault(key, false);
	}
	
	private Parent createContent() throws FileNotFoundException, ClassNotFoundException, IOException {
		appRoot = new Pane();
		gameRoot = new Pane();
		gameRoot.setPrefSize(windowWidth, windowHeight);
		
		jumper = new Jumper();
		scorePanel = new ScorePanel(jumper);
		gameRoot.getChildren().add(jumper);
		generatePlatforms(300);
		
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) { 
				jumper.update();
				//moveGameRoot();
			}	
		};
		
		jumper.translateYProperty().addListener((obs, old, newValue) -> {
			int offset = newValue.intValue();
			if (offset < windowHeight / 3) {
				gameRoot.setLayoutY(-(offset - windowHeight / 3));
			}
		});
		
		background = new ImageView(imgBackground);
		
		appRoot.getChildren().addAll(background, gameRoot, scorePanel);
		
		return appRoot;
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		
		Scene scene = new Scene(createContent());
		scene.setOnKeyPressed(key-> { keys.put(key.getCode(), true); });	
		scene.setOnKeyReleased(key-> { keys.put(key.getCode(), false); });
		
		timer.start();
		
		// Closing window
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				try {
					jumper.save();
				} catch (IOException e) {
					e.printStackTrace();
				}
				stage.close();
			}			
		});
		
		stage.setTitle("Jumper");
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);

	}
}
