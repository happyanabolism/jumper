import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Jetpack extends Bonus {

	public static final double probability = 0.05;
	
	public Jetpack(Platform platform) {
		width = 28;
		height = 32;
		startPosition = randomizePosition(platform);
		setTranslateX(startPosition.getX());
		setTranslateY(startPosition.getY());
		
		
		img = new Image(getClass().getResource("jetpack.png").toString(), width, height, false, false);
		imageBonus = new ImageView(img);
		
		this.getChildren().add(imageBonus);
	}
	
	@Override
	protected void action(Jumper jumper) {
		jumper.fly();
	}

	@Override
	protected Point2D randomizePosition(Platform platform) {
		double x = Math.random() * (platform.getPlatformWidth() - this.width) + platform.getTranslateX();
		double y = platform.getTranslateY() - this.height * 1.2;
		return new Point2D(x, y);
	}

	@Override
	protected void remove() {
		Game.gameRoot.getChildren().remove(this);
		Game.bonuses.remove(this);
		
	}

}
