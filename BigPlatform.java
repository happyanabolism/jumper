import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class BigPlatform extends Platform {

	@Override
	protected void setTexturePlatform(int widthImage, int heightImage) {
		image = new Image(getClass().getResource("ground_grass.png").toString(),
						  widthImage, heightImage, false, false);
		texture = new ImageView(image);
		this.getChildren().add(texture);
	}

	public BigPlatform() {
		width = 200;
		setTexturePlatform(width, height);
	}

	@Override
	public int getPlatformHeight() {
		return height;
	}

	@Override
	public int getPlatformWidth() {
		return width;
	}
}
