public abstract class Coin extends Bonus {
	
	protected SpriteAnimation animation;
	protected final int countFrames = 6;
	protected final int columns = 6;
	protected int offsetX = 0;
	
	protected Coin() {
		width = 20;
		height = 20;
	}
	
	protected void remove() {
		animation.stop();
		Game.bonuses.remove(this);
		Game.gameRoot.getChildren().remove(this);
	}
}