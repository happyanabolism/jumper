import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SilverCoin extends Coin {
	
	static final double probability = 0.3;
	
	public SilverCoin(Platform platform) {
		super();
		img = new Image(getClass().getResource("silver_coin.png").toString(), width * countFrames, height, false, false);
		imageBonus = new ImageView(img);
		
		startPosition = randomizePosition(platform);
		this.setTranslateX(startPosition.getX());
		this.setTranslateY(startPosition.getY());
		
		animation = new SpriteAnimation(imageBonus, Duration.millis(500), countFrames, columns, offsetX, 0, width, height);
		animation.play();
		
		this.getChildren().add(imageBonus);
	}


	private void increment(Jumper jumper) {
		jumper.setSilverCoins(jumper.getSilverCoins() + 1);
		Game.scorePanel.updatePanel(jumper);
	}


	@Override
	protected Point2D randomizePosition(Platform platform) {
		double x = Math.random() * (platform.getPlatformWidth() - this.width) + platform.getTranslateX();
		double y = platform.getTranslateY() - this.height * 2;
		return new Point2D(x, y);
	}


	@Override
	protected void action(Jumper jumper) {
		increment(jumper);
	}
}
