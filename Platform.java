import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public abstract class Platform extends Pane {
	protected int width;
	protected int height = 50;
	protected Image image;
	protected ImageView texture;
	
	public abstract int getPlatformHeight();
	public abstract int getPlatformWidth();	
	protected abstract void setTexturePlatform(int widthImage, int heightImage);
}
