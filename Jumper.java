import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

public class Jumper extends Pane implements Serializable {
	
	// Sizes and position
	private int width = 30;
	private int height = 50;
	private transient Point2D startPoint = new Point2D(Game.windowWidth / 2 - width / 2,
											 Game.windowHeight - height);
	private transient Direction direction;
	private boolean jumping = false;
	private boolean falling = false;
		
	// Speed
	private int gravitationSpeed = -8;
	private int jumpSpeed = 64;
	private final int speed = 10;
	private transient Point2D velocityVector;
	private boolean canJump;
	
	// Coins and score
	private int goldCoins = 0;
	private int silverCoins = 0;
	private int bronzeCoins = 0;
	private int score = 0;
	private int carrots = 0;
	private int lifes = 0;
	
	// Serialization
	private static final long serialVersionUID = 1L;
	
	private transient Image img = new Image(getClass().getResource("bunny2_stand.png").toString(), width, height, false, false);
	private transient Image imgFalling = new Image(getClass().getResource("bunny2_ready.png").toString(), width, height, false, false);
	private transient ImageView jumperImage;
	
	private void TEST() {
		System.out.println(direction.name() + (jumping ? " JUMP " : "") + (falling ? " FALLING " : ""));
	}
	
	// Saving
	public void save() throws IOException {
		ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("jumper.txt"));
		outputStream.writeObject(this);
		outputStream.flush();
		outputStream.close();
	}
	
	// Loading
	private void load() throws FileNotFoundException, IOException, ClassNotFoundException {
		@SuppressWarnings("resource")
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("jumper.txt"));
		Jumper jumper = (Jumper)inputStream.readObject();
		this.goldCoins = jumper.goldCoins;
		this.silverCoins = jumper.silverCoins;
		this.bronzeCoins = jumper.bronzeCoins;
		this.score = jumper.score;
	}
	
	public Jumper() throws FileNotFoundException, ClassNotFoundException, IOException {
		
		jumperImage = new ImageView(img); 
		load();
		
		this.setTranslateX(startPoint.getX());
		this.setTranslateY(startPoint.getY());
		direction = Direction.STAND;
		canJump = true;
		velocityVector = new Point2D(0, 0);
		
		this.getChildren().add(jumperImage);
	}
	
	private void isCollectBonus() {
		for (Bonus bonus : Game.bonuses) {
			if (bonus.getBoundsInParent().intersects(this.getBoundsInParent())) {
				bonus.action(this);
				bonus.remove();
				return;
			}
		}
	}
	
	public void update() {
		
		if (Game.isPressed(KeyCode.RIGHT)) { direction = Direction.RIGHT; }
		if (Game.isPressed(KeyCode.LEFT)) {	direction = Direction.LEFT;	}		
		if (Game.isPressed(KeyCode.SPACE)) { jump(); }
		
		if (direction != Direction.STAND) { moveX(this.direction); }
		if (jumping || falling) { moveY((int)velocityVector.getY()); }
		isCollectBonus();
		
		if (falling || jumping) { enableGravitation(); }
		else { disableGravitation(); }
		
	}

	public void moveX(Direction direction) {
		for (int i = 0; i < speed; i++) {
			for (Platform platform : Game.platforms) {
				if ((getTranslateY() + height + 1 == platform.getTranslateY()) &&
					(getTranslateX() - width < platform.getTranslateX() + 15 ||
					 getTranslateX() > platform.getTranslateX() + platform.getPlatformWidth() - 15)) {
					setFalling(true);
				}
			}
			
			if (getTranslateX() < 0)
				setTranslateX(0);
			else if (getTranslateX() + width > Game.windowWidth)
				setTranslateX(Game.windowWidth - width);
			
			if (direction == Direction.LEFT) {
				setTranslateX(getTranslateX() -1);
			}
			else if (direction == Direction.RIGHT)
			{
				setTranslateX(getTranslateX() + 1);
			}

		}
		
		this.direction = Direction.STAND;
	}
	
	public void moveY(int value) {
		boolean moveDown = value > 0 ? false : true;
		setJumping(!moveDown);
		setFalling(moveDown);

		for (int i = 0; i < Math.abs(value); i++) {
			for (Platform platform : Game.platforms) {
				if (this.getBoundsInParent().intersects(platform.getBoundsInParent()) && getTranslateY() + height == platform.getTranslateY() &&
					this.getTranslateX() + width > platform.getTranslateX() + 8 && this.getTranslateX() < platform.getTranslateX() + platform.getPlatformWidth() - 8) {
						setTranslateY(getTranslateY() - 1);
						setFalling(false);
						
						return;
				}	
			}
			
			if (getTranslateY() + height > Game.windowHeight) {
				setTranslateY(Game.windowHeight - height);
				setJumping(false);
				setFalling(false);
							
				return;
			}
			setTranslateY(getTranslateY() + (moveDown ? 1 : -1));
		}
	}
	
	private void setFalling(boolean isFalling) {
		falling = isFalling;
		if (falling || jumping) {
			jumperImage.setImage(imgFalling);
		}
		else {
			jumperImage.setImage(img);
		}
	}
	
	private void setJumping(boolean isJumping) {
		jumping = isJumping;
		if (jumping || falling) {
			jumperImage.setImage(imgFalling);
		}
		else {
			jumperImage.setImage(img);
		}
	}
	
	public void fly() {
		velocityVector = new Point2D(0, jumpSpeed * 2);
		setJumping(true);
	}
	
	public int getScore() {
		return score;
	}
	
	public void setGoldCoins(int value) {
		goldCoins = value;
	}
	
	public int getGoldCoins() {
		return goldCoins;
	}
	
	public void setSilverCoins(int value) {
		silverCoins = value;
	}
	
	public int getSilverCoins() {
		return silverCoins;
	}
	
	public void setBronzeCoins(int value) {
		bronzeCoins = value;
	}
	
	public int getBronzeCoins() {
		return bronzeCoins;
	}
	
	public void changeDirection(Direction direction) {
		this.direction = direction;
	}
	
	private void enableGravitation() {
		if (velocityVector.getY() > gravitationSpeed) {
			velocityVector = velocityVector.add(0, gravitationSpeed);
		}
	}
	
	private void disableGravitation() {
		velocityVector = new Point2D(0, 0);
	}
	
	public void jump() {
		if (!falling && !jumping) {
			velocityVector = velocityVector.add(0, jumpSpeed);
			setJumping(true);
		}
	}
}
