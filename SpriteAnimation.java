import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SpriteAnimation extends Transition {
	private final ImageView imageView;
	private final int count;
	private final int columns;
	private int offsetX;
	private int offsetY;
	private final int width;
	private final int height;
	
	public SpriteAnimation(
			ImageView imageView,
			Duration duration,
			int count, int columns,
			int offsetX, int offsetY,
			int width, int height) {
		
		this.imageView = imageView;
		this.count = count;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.columns = columns;
		this.width = width;
		this.height = height;
		setCycleDuration(duration);
		setInterpolator(Interpolator.LINEAR);
		setCycleCount(Animation.INDEFINITE);
	}
	
	public void setOffsetX(int x) {
		this.offsetX = x;
	}
	
	public void setOffsetY(int y) {
		this.offsetY = y;
	}
	
	@Override
	protected void interpolate(double k) {
		int index = Math.min((int) Math.floor(count * k), count - 1);
		int x = (index % columns) * width + offsetX;
		int y = (index / columns) * height + offsetY;
		imageView.setViewport(new Rectangle2D(x, y, width, height));
	}
}
