import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;

public class ScorePanel extends Pane {

	private final int width = Game.windowWidth;
	private final int height = 15;
	
	Image img = new Image(getClass().getResource("score_panel.png").toString(), width, height , false, false);
	ImageView scorePanelImage = new ImageView(img);
	Rectangle rect = new Rectangle(Game.windowWidth, 15);
	
	private Label goldCoinsLabel;	
	private Label silverCoinsLabel;
	private Label bronzeCoinsLabel;
	private Label carrotsLabel;
	private Label lifesLabel;
	
	public ScorePanel(Jumper jumper) {
		rect.setFill(Color.GRAY);
		
		goldCoinsLabel = new Label();
		goldCoinsLabel.setTranslateX(60);
		goldCoinsLabel.setText("" + jumper.getGoldCoins());
		goldCoinsLabel.setTextFill(Color.GOLD);
		
		silverCoinsLabel = new Label();
		silverCoinsLabel.setTranslateX(180);
		silverCoinsLabel.setText("" + jumper.getSilverCoins());
		silverCoinsLabel.setTextFill(Color.SILVER);
		
		bronzeCoinsLabel = new Label();
		bronzeCoinsLabel.setTranslateX(300);
		bronzeCoinsLabel.setText("" + jumper.getBronzeCoins());
		bronzeCoinsLabel.setTextFill(Color.WHITESMOKE);
		
		this.getChildren().addAll(rect, scorePanelImage, goldCoinsLabel, silverCoinsLabel, bronzeCoinsLabel);
	}


	public void updatePanel(Jumper jumper) {
		goldCoinsLabel.setText("" + jumper.getGoldCoins());
		silverCoinsLabel.setText("" + jumper.getSilverCoins());
		bronzeCoinsLabel.setText("" + jumper.getBronzeCoins());
	}
	
}
